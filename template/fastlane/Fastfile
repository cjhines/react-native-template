# This file contains the fastlane.tools configuration
# You can find the documentation at https://docs.fastlane.tools
#
# For a list of all available actions, check out
#
#     https://docs.fastlane.tools/actions
#
# For a list of all available plugins, check out
#
#     https://docs.fastlane.tools/plugins/available-plugins
#

# Uncomment the line if you want fastlane to automatically update itself
# update_fastlane

default_platform(:ios)

platform :ios do

  before_all do 
    # Increase the default timeout of 3
    # This is sometimes needed when there are several complex dependencies
    ENV["FASTLANE_XCODEBUILD_SETTINGS_TIMEOUT"] = "120"

    # Handle the keychain in a non-invasive way, inspired by setup_ci.
    # (but without setting the temp keychain as default)
    # https://github.com/fastlane/fastlane/blob/3f134ee01eac331daf92b31653f3f08651d0f04c/fastlane/lib/fastlane/actions/setup_ci.rb#L26
    unless is_ci 
      UI.message("Not running on CI, skipping CI setup")
      next 
    end 

    keychain_name = "fastlane_tmp_keychain"
    ENV["MATCH_KEYCHAIN_NAME"] = keychain_name
    ENV["MATCH_KEYCHAIN_PASSWORD"] = ""

    # Add keychains fresh to avoid collection of various certificates for multiple users each build.
    UI.message("Creating temporary keychain: \"#{keychain_name}\".")
    create_keychain(
      name: keychain_name,
      default_keychain: false,
      unlock: true,
      timeout: 3600,
      lock_when_sleeps: true,
      password: "",
      add_to_search_list: true
    )

    UI.message("Enabling match readonly mode.")
    ENV["MATCH_READONLY"] = true.to_s
  end
  
  after_all do 
    unless is_ci
      next
    end

    UI.message("Deleting temporary keychain: \"#{ENV["MATCH_KEYCHAIN_NAME"]}\".")
    delete_keychain(name: ENV["MATCH_KEYCHAIN_NAME"])
  end

  lane :archive do 
    build_ios_app(
      scheme: "myapp",
      skip_package_ipa: true,
      output_directory: "ios",
      workspace: "ios/myapp.xcworkspace",
      archive_path: "ios/myapp.xcarchive",
      suppress_xcode_output: true,
      silent: true
    )
  end

  lane :export_adhoc do
    match(type: "adhoc", readonly: true)
    build_app(
      export_method: "ad-hoc",
      skip_build_archive: true,
      workspace: "ios/myapp.xcworkspace",
      archive_path: "ios/myapp.xcarchive",
      output_directory: "ios/ad-hoc",
      suppress_xcode_output: true,
      include_bitcode: false,
      include_symbols: false,
      silent: true
    )
  end

  lane :export_appstore do
    match(type: "appstore", readonly: true)
    build_app(
      export_method: "app-store",
      skip_build_archive: true,
      workspace: "ios/myapp.xcworkspace",
      archive_path: "ios/myapp.xcarchive",
      output_directory: "ios/appstore",
      suppress_xcode_output: true,
      silent: true
    )
  end

  lane :code_signing do
    match(type: "development", readonly: true)
    bundle_identifier = CredentialsManager::AppfileConfig.try_fetch_value(:app_identifier)
    update_code_signing_settings(
      path: "ios/myapp.xcodeproj",
      use_automatic_signing: false,
      bundle_identifier: bundle_identifier,
      profile_uuid: ENV["sigh_" + bundle_identifier + "_development"], 
      team_id: CredentialsManager::AppfileConfig.try_fetch_value(:team_id)
    )
  end

  lane :build_release do
    code_signing
    archive
    export_adhoc
    export_appstore
  end

end


platform :android do
  lane :build_release do |options|
    gradle(
      tasks: ["clean", "assembleRelease", "bundleRelease" ],
      project_dir: "android/",
      properties: {
        "android.injected.signing.store.file" => File.join(Dir.pwd, "upload.keystore"),
        "android.injected.signing.store.password" => options[:keystore_password],
        "android.injected.signing.key.alias" => "myapp",
        "android.injected.signing.key.password" => options[:keystore_password]
      }
    )
  end
end
