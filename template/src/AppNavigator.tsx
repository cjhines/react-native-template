import {
  NavigationContainer,
  createNavigationContainerRef,
} from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import * as Sentry from '@sentry/react-native';
import * as React from 'react';

import SampleScreenOne from './screens/SampleScreenOne';
import SampleScreenTwo from './screens/SampleScreenTwo';

export type AppStackParamList = {
  SampleOne: undefined;
  SampleTwo: undefined;
};

const Stack = createNativeStackNavigator<AppStackParamList>();

const navigationIntegration = Sentry.reactNavigationIntegration({
  enableTimeToInitialDisplay: true,
});

const AppNavigator = () => {
  const navigationRef = createNavigationContainerRef<AppStackParamList>();

  const onReady = () => {
    if (process.env.EXPO_PUBLIC_SENTRY_DSN) {
      navigationIntegration.registerNavigationContainer(navigationRef);
    }
  };

  return (
    <NavigationContainer ref={navigationRef} onReady={onReady}>
      <Stack.Navigator id={undefined} screenOptions={{ headerShown: false }}>
        <Stack.Screen name="SampleOne" component={SampleScreenOne} />
        <Stack.Screen name="SampleTwo" component={SampleScreenTwo} />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default AppNavigator;
