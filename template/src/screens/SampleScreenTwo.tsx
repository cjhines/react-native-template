import { useNavigation } from '@react-navigation/native';
import { NativeStackNavigationProp } from '@react-navigation/native-stack';
import React, { FunctionComponent } from 'react';
import { FormattedMessage } from 'react-intl';
import { Button, StyleSheet, View } from 'react-native';

import { AppStackParamList } from '../AppNavigator';
import AppText from '../components/AppText';

type Navigation = NativeStackNavigationProp<AppStackParamList, 'SampleOne'>;

const SampleScreenTwo: FunctionComponent = () => {
  const navigation = useNavigation<Navigation>();

  return (
    <View style={styles.container}>
      <AppText type="BodyLarge">
        <FormattedMessage
          defaultMessage="Sample Screen 2"
          description="SampleScreenTwo - screen title"
        />
      </AppText>
      <Button
        title="Back to Screen 1"
        onPress={() => navigation.navigate('SampleOne')}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
  },
});

export default SampleScreenTwo;
