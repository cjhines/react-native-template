import { useNavigation } from '@react-navigation/native';
import { NativeStackNavigationProp } from '@react-navigation/native-stack';
import React, { FunctionComponent } from 'react';
import { FormattedMessage } from 'react-intl';
import { Button, StyleSheet, View } from 'react-native';

import { AppStackParamList } from '../AppNavigator';
import AppText from '../components/AppText';
import { ToastType } from '../components/AppToast/types';
import useToast from '../hooks/useToast';

type Navigation = NativeStackNavigationProp<AppStackParamList, 'SampleOne'>;

const SampleScreenOne: FunctionComponent = () => {
  const navigation = useNavigation<Navigation>();
  const toast = useToast();

  const showToast = () => {
    toast.show('Here is an example toast', { type: ToastType.DEFAULT });
  };

  return (
    <View style={styles.container}>
      <AppText type="BodyLarge">
        <FormattedMessage
          defaultMessage="Sample Screen 1"
          description="SampleScreenOne - screen title"
        />
      </AppText>
      <Button
        title="Go to Screen 2"
        onPress={() => navigation.navigate('SampleTwo')}
      />
      <Button title="Test toast" onPress={showToast} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    justifyContent: 'center',
    gap: 16,
    flex: 1,
  },
});

export default SampleScreenOne;
