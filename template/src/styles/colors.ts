const primitives = {
  NEUTRAL_LIGHT: '#FFFFFF',
  NEUTRAL_DARK: '#000000',
};

const tokens = {
  BACKGROUND_DEFAULT: primitives.NEUTRAL_LIGHT,
  TEXT_DEFAULT: primitives.NEUTRAL_DARK,
};

export default tokens;
