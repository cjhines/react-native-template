# Styles folder

## Purpose

This folder contains style constants imported from Figma. It should act as a reflection of the design team's design tokens.


## Anti-patterns

- Adding shared StyleSheet declarations
- Adding color/font/spacing objects or properties which do not exist in Figma
