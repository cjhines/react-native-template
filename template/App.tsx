import * as Sentry from '@sentry/react-native';
import React from 'react';
import { IntlProvider } from 'react-intl';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import { Provider as StoreProvider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';

import AppNavigator from './src/AppNavigator';
import { ToastProvider } from './src/hooks/useToast';
import { getCurrentTranslation } from './src/intl';
import { persistor, store } from './src/store';

const sentryDSN = process.env.EXPO_PUBLIC_SENTRY_DSN;

if (sentryDSN) {
  Sentry.init({
    dsn: sentryDSN,
    // Set tracesSampleRate to 1.0 to capture 100% of transactions for tracing.
    // We recommend adjusting this value in production.
    // Learn more at
    // https://docs.sentry.io/platforms/javascript/configuration/options/#traces-sample-rate
    tracesSampleRate: 1.0,
    // profilesSampleRate is relative to tracesSampleRate.
    // Here, we'll capture profiles for 100% of transactions.
    profilesSampleRate: 1.0,
  });
}

const App = () => {
  // Here you can supply the current and default language used for translation.
  // This can come from a store value or a library implementation such as expo-localization.
  const language = 'en';

  return (
    <StoreProvider store={store}>
      <IntlProvider
        messages={getCurrentTranslation(language)}
        defaultLocale="en"
        locale={language}>
        <SafeAreaProvider>
          <ToastProvider>
            <PersistGate persistor={persistor}>
              <AppNavigator />
            </PersistGate>
          </ToastProvider>
        </SafeAreaProvider>
      </IntlProvider>
    </StoreProvider>
  );
};

export default App;
