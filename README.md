<picture>
  <img alt="Hybrid Heroes React Native Template" src="./docs/hero.webp" style="display: block; margin: 0 auto; max-width: 70%; height: auto;" />
</picture>


# :atom_symbol: Hybrid Heroes React Native Template

An extended template by [Hybrid Heroes](https://hybridheroes.de/). Elegant usage directly within the [React Native CLI](https://github.com/react-native-community/cli).

## :star: Extended features

- [Expo](https://expo.dev/) SDK + CLI
- Extended [ESLint](https://eslint.org/) & [Prettier](https://prettier.io/) configurations
- [Husky](https://typicode.github.io/husky) pre-commit setup
  - [lint-staged](https://github.com/okonet/lint-staged)
  - [commitlint](https://commitlint.js.org/)
- [Node Version Manager](https://github.com/nvm-sh/nvm) configuration file: ([`.nvmrc`](template/.nvmrc))
- [React Navigation](https://reactnavigation.org/) configuration.
- [Redux](https://redux.js.org/) configuration:
  - [Redux Toolkit](https://redux-toolkit.js.org/)
  - [React Redux](https://react-redux.js.org/)
  - [Redux Persist](https://github.com/rt2zz/redux-persist)
- [React Intl / Format.JS](https://formatjs.io/docs/react-intl/)

## :heavy_check_mark: Usage

```sh
npx @react-native-community/cli@latest init MyApp --pm npm --template https://gitlab.com/hybridheroes/opensource/react-native-template --install-pods false
```

**Note:** `yarn`, `npm`, or `bun` can be provided to the `--pm` option

## :warning: Expo CLI

This template only works with Expo CLI. Make sure you have uninstalled the legacy `react-native-cli` first (`npm uninstall -g react-native-cli`) for the below command to work.

# Documentation

## :arrow_up: Updating dependencies

For updating React Native, please refer to the [official React Native template](https://github.com/facebook/react-native/tree/main/packages/react-native/template). Don't forget to select the desired release tag.

For updating other dependencies, you can run the following command:

```
npm install @react-navigation/native@latest @react-navigation/native-stack@latest @reduxjs/toolkit@latest react-native-gesture-handler@latest react-native-mmkv@latest react-native-safe-area-context@latest react-native-screens@latest react-redux@latest redux-persist@latest @commitlint/cli@latest @commitlint/config-conventional@latest @formatjs/cli@latest husky@latest lint-staged@latest
```

## :gem: RVM & NVM

The Template comes with a `.nvmrc` (currently node 18) so you can use `nvm use` bash command.
You can also use `rvm use` bash command. The Ruby version is defined in the `Gemfile` (currently version 2.7.4)

## :computer: Contributing

Contributions are very welcome. Please check out the [contributing document](CONTRIBUTING.md).

## :bookmark: License

This project is [MIT](LICENSE) licensed.
