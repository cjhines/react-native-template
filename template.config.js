module.exports = {
  placeholderName: 'HelloWorld',
  titlePlaceholder: 'Hello App Display Name',
  templateDir: './template',
  postInitScript: './postInitScript.js'
}
